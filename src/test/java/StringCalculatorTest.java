import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    @Test
    void shouldReturnZeroWithAnEmptyInputString() {
        assertEquals(0,StringCalculator.Add(""));
    }

    @Test
    void shouldReturnNumberWithOneNum(){
        assertEquals(1,StringCalculator.Add("1"));
    }
    @Test
    void shouldReturnNumberSumWIthTwoNum(){
        assertEquals(3,StringCalculator.Add("1,2"));
    }
    @Test
    void handleUnknownNumberAmountNumbers(){
        assertEquals(15,StringCalculator.Add("1,2,3,4,5"));
    }
    @Test
    void handleNewLinesBetweenNumbers(){
        assertEquals(6,StringCalculator.Add("1\n2,3"));
    }
    @Test
    void handleDifferentDelimiters(){
        assertEquals(6,StringCalculator.Add("\\;\n1\n2;3"));
    }
    @Test
    void shouldRaiseExceptionOnNegatives(){
        try{
            StringCalculator.Add("-1,2,3");
            fail("Exception expected.");
        }
        catch (RuntimeException e)
        {
            assertEquals("Unsupported negatives[-1]",e.getMessage());
        }
    }

    @Test
    void shouldRaiseExceptionOnMultiNegatives(){
        try{
            StringCalculator.Add("-1,-2,-3");
            fail("Exception expected.");
        }
        catch (RuntimeException e)
        {
            assertEquals("Unsupported negatives[-1, -2, -3]",e.getMessage());
        }
    }
}