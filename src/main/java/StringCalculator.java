import io.cucumber.java.sl.In;

import java.util.ArrayList;
import java.util.Arrays;

public class StringCalculator {
    public static int Add(String equ)  {
        if (equ.isEmpty())
            return 0;
        return calculate(split(equ));
    }

    public static int[] split(String equ) throws RuntimeException {
        if (equ.startsWith("\\")){
            char div = equ.charAt(1);
            return  Arrays.stream(equ.substring(3).split(div+"|\n")).mapToInt(Integer::parseInt).toArray();
        }
        else return  Arrays.stream(equ.split(",|\n")).mapToInt(Integer::parseInt).toArray();
    }

    public static int calculate(int[] ints) throws RuntimeException {
        int temp = 0;
        ArrayList<Integer> negatives = new ArrayList<>();
        for (int i :ints) {
            if (i<0)  negatives.add(i);
            else temp+=i;
        }
        if (negatives.size()>0) throw new RuntimeException("Unsupported negatives"+negatives.toString());
        return  temp;
    }
}
